﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BLL;
using DAL;

namespace PersonalTracking
{
    public partial class FrmDeparment : Form
    {
        public FrmDeparment()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DEPARTMENT deparment  = new DEPARTMENT();
            deparment.DepartamentName = textBoxDeparment.Text;
            BLL.DepartmentBill.AddDeparment(deparment);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmDeparment_Load(object sender, EventArgs e)
        {

        }
    }
}
